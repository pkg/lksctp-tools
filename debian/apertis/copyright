Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2007, Hewlett-Packard Development Company, L.P.
 2002, Nokia, La Monte Yarroll, Intel.
 2001, Motorola, Cisco, Intel, Nokia, La Monte Yarroll.
 2001, 2003, IBM Corp.
License: GPL-2+

Files: debian/*
Copyright: 2003, Hewlett-Packard Development Company, L.P
 2001, Nokia, Inc.
 2001, La Monte H.P. Yarroll
 2001, Intel Corp.
 2001, 2003, 2004, IBM Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, 2003, Cisco, Inc.
License: GPL-2+

Files: man/*
Copyright: 1999-2000, 2003 Cisco, Inc.
 1999-2001 Motorola, Inc.
 2001 Nokia, Inc.
 2001 Intel Corp.
 2001, 2003, 2004 IBM Corp.
 2003 Hewlett-Packard Development Company, L.P
 2001 La Monte H.P. Yarroll
License: GPL-2.0+

Files: doc/template.c
Copyright: 2001, Nokia, Inc.
 2001, La Monte H.P. Yarroll
 2001, International Business Machines, Corp.
 2001, Intel Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, Cisco, Inc.
License: GPL-2+

Files: lksctp-tools.spec.in
Copyright: 2004, IBM Corp.
 2004, Hewlett-Packard Company
License: GPL-2 or LGPL

Files: src/*
Copyright: 2004, IBM Corp.
 2003, Hewlett-Packard Development Company, L.P
License: GPL-2+

Files: src/apps/bindx_test.c
Copyright: 2003, IBM Corp.
 2002, Intel Corporation
License: GPL-2

Files: src/apps/myftp.c
Copyright: 2002, Intel Corp.
License: GPL-2+

Files: src/apps/nagle_rcv.c
 src/apps/nagle_snd.c
Copyright: 2001-2004, IBM Corp.
 2001, Nokia, Inc.
 2001, Intel Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, Cisco, Inc.
License: GPL-2+

Files: src/apps/peel_client.c
Copyright: 2003, IBM Corp.
License: BSD-3-clause or GPL-2+

Files: src/apps/peel_server.c
Copyright: 2003, IBM Corp.
 2003, Cisco
License: BSD-3-clause or GPL-2+

Files: src/apps/sctp_darn.c
 src/apps/sctp_test.c
Copyright: 2001, La Monte H.P. Yarroll
 2001, 2003, IBM Corp.
 2001, 2002, Nokia
 1999-2001, Motorola
 1999, Cisco
License: GPL-2+

Files: src/apps/sctp_status.c
Copyright: 2008, 2009, Fujitsu Ltd.
License: GPL-2+

Files: src/apps/sctp_xconnect.c
Copyright: 2003, IBM Corp.
License: GPL-2+

Files: src/func_tests/test_assoc_abort.c
 src/func_tests/test_assoc_shutdown.c
 src/func_tests/test_autoclose.c
 src/func_tests/test_basic.c
 src/func_tests/test_fragments.c
 src/func_tests/test_peeloff.c
 src/func_tests/test_sockopt.c
 src/func_tests/test_timetolive.c
Copyright: 2001-2004, IBM Corp.
 2001, Nokia, Inc.
 2001, Intel Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, Cisco, Inc.
License: GPL-2+

Files: src/func_tests/test_connect.c
 src/func_tests/test_connectx.c
 src/func_tests/test_getname.c
 src/func_tests/test_recvmsg.c
 src/func_tests/test_tcp_style.c
Copyright: 2002-2004, IBM Corp.
 1999-2001, Motorola, Inc.
License: GPL-2+

Files: src/func_tests/test_inaddr_any.c
Copyright: 2001-2003, IBM Corp.
 2001, Nokia, Inc.
 2001, La Monte H.P. Yarroll
 2001, Intel Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, Cisco, Inc.
License: GPL-2+

Files: src/func_tests/test_sctp_sendrecvmsg.c
Copyright: 2003, Intel Corp.
 2003, IBM Corp.
License: GPL-2+

Files: src/func_tests/test_sctp_sendvrecvv.c
Copyright: 2018, REDHAT Corp.
License: GPL-2+

Files: src/include/netinet/sctp.h.in
Copyright: 2001, 2004, IBM Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, Cisco, Inc.
License: LGPL-2.1

Files: src/lib/addrs.c
Copyright: 2003, IBM Corp.
 2001, 2002, Intel Corp.
License: LGPL-2.1

Files: src/lib/bindx.c
 src/lib/opt_info.c
Copyright: 2002, Intel Corporation.
 2001, 2003, IBM Corp.
License: LGPL-2.1

Files: src/lib/connectx.c
 src/lib/peeloff.c
Copyright: 2001, 2003, 2005, IBM Corp.
License: LGPL-2.1

Files: src/lib/recvmsg.c
Copyright: 2003, International Business Machines, Corp.
License: LGPL-2.1

Files: src/lib/sendmsg.c
Copyright: 2003, Intel Corp.
License: LGPL-2.1

Files: src/testlib/sctputil.c
Copyright: 2001, La Monte H.P. Yarroll
 2001, 2003, IBM Corp.
 2001, 2002, Nokia
 1999-2001, Motorola
 1999, Cisco
License: GPL-2+

Files: src/testlib/sctputil.h
Copyright: 2001-2003, IBM Corp.
 2001, Nokia, Inc.
 2001, La Monte H.P. Yarroll
 2001, Intel Corp.
 1999-2001, Motorola, Inc.
 1999, 2000, Cisco, Inc.
License: GPL-2+

Files: src/withsctp/*
Copyright: 2003, La Monte HP Yarroll <piggy@acm.org>
License: BSD-3-clause

Files: configure.ac man/sctp_connectx.3 man/sctp_recvv.3 man/sctp_send.3 man/sctp_sendv.3
Copyright: 1999-2000, 2003 Cisco, Inc.
 1999-2001 Motorola, Inc.
 2001 Nokia, Inc.
 2001 Intel Corp.
 2001, 2003, 2004 IBM Corp.
 2003 Hewlett-Packard Development Company, L.P
 2001 La Monte H.P. Yarroll
License: GPL-2.0+
